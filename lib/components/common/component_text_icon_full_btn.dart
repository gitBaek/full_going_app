import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_margin_horizon.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/enums/enum_size.dart';

class ComponentTextIconFullBtn extends StatelessWidget {
  final Color color;
  final IconData icon;
  final String text;
  final VoidCallback callback;

  const ComponentTextIconFullBtn(this.color, this.icon, this.text, this.callback, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        margin: const EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: colorLightGray),
          borderRadius: const BorderRadius.all(Radius.circular(5)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: color,
              size: 18,
            ),
            const ComponentMarginHorizon(enumSize: EnumSize.small,),
            Text(
              text,
              style: TextStyle(
                color: color,
                fontSize: fontSizeMid,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
