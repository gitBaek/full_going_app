import 'package:dio/dio.dart';
import 'package:full_going_app/config/config_api.dart';
import 'package:full_going_app/functions/token_lib.dart';
import 'package:full_going_app/model/common_result.dart';
import 'package:full_going_app/model/kick_board_end_request.dart';
import 'package:full_going_app/model/kick_board_start_request.dart';
import 'package:full_going_app/model/near_kick_board_list_result.dart';

class RepoKickboard {
  Future<NearKickBoardListResult> getList(double posX, double posY) async {
    final String _baseUrl = '$apiUri/kick-board/near?distanceKm=1&posX={posX}&posY={posY}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{posX}', posX.toString()).replaceAll('{posY}', posY.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return NearKickBoardListResult.fromJson(response.data);
  }

  Future<CommonResult> setStart(int kickBoardId, KickBoardStartRequest request) async {
    final String _baseUrl = '$apiUri/kick-board-history/start-use/kick-board-id/{kickBoardId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
        _baseUrl.replaceAll('{kickBoardId}', kickBoardId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }


  Future<CommonResult> putEnd(int historyId, KickBoardEndRequest request) async {
    final String _baseUrl = '$apiUri/kick-board-history/end-use/history-id/{historyId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        _baseUrl.replaceAll('{historyId}', historyId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }


}