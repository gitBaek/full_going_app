import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/login_check.dart';
import 'package:full_going_app/pages/admin/page_admin_member_list.dart';
import 'package:full_going_app/pages/kikboardUsing/page_end_kick_board.dart';
import 'package:full_going_app/pages/kikboardUsing/page_using_kick_board.dart';
import 'package:full_going_app/pages/menu/page_how_to_use.dart';
import 'package:full_going_app/pages/menu/page_my_charge_history.dart';
import 'package:full_going_app/pages/menu/page_my_use_history.dart';
import 'package:full_going_app/pages/menu/page_my_use_history_detail.dart';

import 'pages/member/page_join.dart';
import 'pages/menu/page_charge_amount.dart';
import 'pages/menu/page_full_going_pass.dart';
import 'pages/menu/page_info.dart';
import 'pages/page_home.dart';
import 'pages/page_test.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FullGoingApp',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: colorBackGround,
      ),
       home: LoginCheck(),         // 로그인 체크 - 완료
      // home: PageLogin(),          // 로그인 - 완료
      // home: PageFindPassword(),   // 비밀번호 찾기 - 완료
       //home: PageJoin(),           // 회원가입 - 완료

      //home: PageProfile(),        // 프로필 - UI 완료 -> 사진 저장 로직 테스트 필요
      //home: PageProfileFix(),     // 비밀번호 변경 - 완료
      //home: PageLicenseReg(),     // 면허 등록 - 완료

      // home: PageHome(),           // 홈
      // home: PageMyUseHistory(),   // 이용 기록 - 이용기록 상세내역 - 완료 완료

      // home: PageFullGoingPass(),   // 패스 - 완료
      // home: PageChargeAmount(),   // 금액충전 - 완료
      // home: PageMyChargeHistory(),   // 충전내역 - 완료
      // home: PageHowToUse(),         // 이용방법 - 완료

      // home: PageInfo(),           // 고객지원 - 완료

      // home: PageUsingKickBoard(), // 킥보드 사용 - UI 완료 ->
      // home: PageEndKickBoard(),     // 킥보드 사용 내역 - UI 완료 ->


      // home: PageAdminMemberList(),  // 관리자 회원리스트
      // 관리자 회원 리스트 상세
      // home: PageKickBoardList(),  // 관리자 킥보드 리스트 (확인 하여야함)
      // 관리자 킥보드 리스트 상세
      // home: PageStatistical(),    // 관리자 매출통계
       //home: PageTest(),             // 테스트

    );
  }
}
