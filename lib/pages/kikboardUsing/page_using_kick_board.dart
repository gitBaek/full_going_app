import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_actions.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_custom_loading.dart';
import 'package:full_going_app/components/common/component_margin_horizon.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/components/component_divider.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/functions/geo_check.dart';
import 'package:full_going_app/model/kick_board_end_request.dart';
import 'package:full_going_app/model/using_kick_board_item_result.dart';
import 'package:full_going_app/pages/kikboardUsing/page_end_kick_board.dart';
import 'package:full_going_app/repository/repo_kickboard.dart';
import 'package:full_going_app/repository/repo_kickboard_history.dart';
import 'package:geolocator/geolocator.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class PageUsingKickBoard extends StatefulWidget {
  const PageUsingKickBoard({super.key});

  @override
  State<PageUsingKickBoard> createState() => _PageUsingKickBoardState();
}

class _PageUsingKickBoardState extends State<PageUsingKickBoard> {
  Position? _currentPosition;

  late Timer _timer;
  int _percentCounter = 0;
  double _percentValue = 0.0;
  bool _isTimerStatus = false;
  int _secCount = 0;
  int _min = 0;
  int _sec = 0;
  int _usedMin = 0;
  String _pauseText = "일시정지";
  String _kickboardText = "킥보드 사용중";
  bool _ispause = false;
  int _kickboardTextCount = 0;

  String dateStart = "";
  int historyId = 0;
  String kickBoardModelName = "";
  num priceBase = 0;
  num priceMinute = 0;
  String priceName = "";
  num startPosX = 0;
  num startPosY = 0;

  Future<void> _putUseEnd(int kickBoardId, KickBoardEndRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoKickboard().putEnd(kickBoardId, request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '사용 종료 완료',
        subTitle: '킥보드 사용이 종료 되었습니다.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  PageEndKickBoard(historyId: historyId)),
          (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '사용 종료 실패',
        subTitle: '킥보드 사용 종료가 실패 했습니다.',
      ).call();
    });
  }

  Future<Position> _getPosition() async {
    try {
      Position position = await GeoCheck.determinePosition();

      setState(() {
        this._currentPosition = position;
      });

      return position;
    } catch (e) {
      ComponentNotification(
        success: false,
        title: '나의 위치 전송 실패',
        subTitle: e.toString(),
      ).call();

      return Future.error('위치 로딩 실패');
    }
  }

  Future<void> _loadKickBoardData() async {
    UsingKickBoardItemResult result =
        await RepoKickboardHistory().getUsingKickBoard();
    setState(() {
      dateStart = result.data.dateStart;
      historyId = result.data.historyId;
      kickBoardModelName = result.data.kickBoardModelName;
      priceBase = result.data.priceBase;
      priceMinute = result.data.priceMinute;
      priceName = result.data.priceName;
      startPosX = result.data.startPosX;
      startPosY = result.data.startPosY;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _startTimer();
    _loadKickBoardData();
    _getPosition();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }

  String _format(int seconds) {
    /*
    * 기본표시 0:25:00.000000
    * 이후 분 : 초 만 가져오기 위해서 toString().split('.').first으로 시 : 분 : 초의 문자열만 남긴다.
    * (0:25:00) 마지막으로 substring(2,7)을 통해 분 : 초만 남게 된다. (25:00)
    * => 다음의 과정을 통해 format 함수는 sec를 인자로 받아, 분 : 초 의 문자열을 리턴한다.
    * 이는 남은 시간을 표시하는 데에 사용된다. Text(format(totalTime))
    * */
    var duration = Duration(seconds: seconds);
    _min = int.parse(duration.toString().split('.').first.substring(2, 4));
    _sec = int.parse(duration.toString().split('.').first.substring(5, 7));

    _usedMin = _sec > 0 ? _min + 1 : _min;

    // print("${_min} : ${_sec}");
    // print("사용 시간 : ${_usedMin} 분");

    return duration.toString().split('.').first.substring(2, 7);
  }

  void _startTimer() {
    if (!_isTimerStatus) {
      _isTimerStatus = true;
      _timer = Timer.periodic(Duration(milliseconds: 1000), (timer) {
        setState(() {
          _secCount++;
          if (_percentCounter >= 59) {
            _percentCounter = 0;
          } else {
            _percentCounter++;
          }
          //100% 60등분 하여 원형 프로그래스 값 계산
          _percentValue = ((100 / 60) * _percentCounter) / 100;
          //print(_percentCounter);

          /* 킥보드 사용중 도트(.) 증가 부분 10초 동안 초당 한번씩 도트(.) 증가 */
          if (!_ispause) {
            if (_kickboardTextCount++ < 9) {
              _kickboardText += '.';
            } else {
              _kickboardText = "킥보드 사용중";
              _kickboardTextCount = 0;
            }
          } else {
            _kickboardTextCount = 0;
          }
        });
      });
    }
  }

  void _stopTimer() {
    _isTimerStatus = false;
    _timer.cancel();
  }

  void _initTimerCount() {
    setState(() {
      _percentCounter = 0;
      _percentValue = 0.0;
      _secCount = 0;
      _min = 0;
      _sec = 0;
    });
  }

  _pauseShowDialog({
    required String titleText,
    required Color titleColor,
    required String contentText,
    required Color contentColor,
    required VoidCallback callbackOk,
    required VoidCallback callbackCancel,
  }) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.report_problem_outlined, color: titleColor),
                ComponentMarginHorizon(enumSize: EnumSize.mid),
                Text(titleText,
                    style: TextStyle(
                        color: titleColor,
                        fontSize: fontSizeBig,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            content: Text(contentText,
                style: TextStyle(
                    color: contentColor,
                    fontSize: fontSizeMid,
                    fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                child: Text("확인"),
                onPressed: callbackOk,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:ComponentAppbarActions(title: "킥보드 사용",),
        body: SingleChildScrollView(
          child: Container(
            padding: bodyPaddingLeftRight,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Text(_kickboardText,
                    style: TextStyle(
                        fontSize: fontSizeSuper, fontWeight: FontWeight.bold)),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.sizeOf(context).width / 10),
                      child: Column(
                        children: [
                          Text(kickBoardModelName,
                              style: TextStyle(
                                  fontSize: fontSizeBig,
                                  fontWeight: FontWeight.w600)),
                          const ComponentMarginVertical(),
                          Image.asset("assets/img_kickboard_loading.png",
                              width: 80),
                        ],
                      ),
                    ),
                    const ComponentMarginHorizon(enumSize: EnumSize.bigger),
                    Container(
                      padding: EdgeInsets.only(
                        right: MediaQuery.sizeOf(context).width / 10,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(priceName,
                              style: TextStyle(
                                  fontSize: fontSizeMid,
                                  fontWeight: FontWeight.w600,
                                  color: colorDarkGray)),
                          const ComponentMarginVertical(),
                          Text(
                              "잠금해제 : " +
                                  maskWonFormatter.format(priceBase) +
                                  " 원",
                              style: TextStyle(
                                  fontSize: fontSizeSm,
                                  fontWeight: FontWeight.w600)),
                          Text(
                              "분당 : " +
                                  maskWonFormatter.format(priceMinute) +
                                  " 원",
                              style: TextStyle(
                                  fontSize: fontSizeSm,
                                  fontWeight: FontWeight.w600)),
                        ],
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                ComponentDivider(),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                GestureDetector(
                    child: CircularPercentIndicator(
                        radius: 100.0,
                        animation: true,
                        animationDuration: 1000,
                        lineWidth: 20.0,
                        percent: _percentValue,
                        reverse: false,
                        startAngle: 0.0,
                        animateFromLastPercent: true,
                        circularStrokeCap: CircularStrokeCap.round,
                        backgroundColor: Colors.grey,
                        linearGradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          tileMode: TileMode.clamp,
                          stops: [0.0, 1.0],
                          colors: <Color>[
                            Colors.greenAccent,
                            Colors.blueAccent,
                          ],
                        ),
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.timer_sharp,
                              size: 30,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(_format(_secCount),
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontWeight: FontWeight.bold)),
                          ],
                        )),
                    onTap: () {
                      !_isTimerStatus ? _startTimer() : _stopTimer();
                    },
                    onDoubleTap: () {
                      //_initTimerCount();
                    }),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                ComponentDivider(),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.sizeOf(context).width / 10),
                      child: Column(
                        children: [
                          Icon(
                            Icons.timer_sharp,
                            size: 30,
                          ),
                          SizedBox(height: 10),
                          Icon(
                            Icons.monetization_on_outlined,
                            size: 30,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          right: MediaQuery.sizeOf(context).width / 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "${_usedMin} 분",
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: fontSizeSuper),
                          ),
                          SizedBox(height: 10),
                          Text(
                            maskWonFormatter.format(_usedMin * 100) + " 원",
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: fontSizeSuper),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.bigger),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: ComponentTextBtn(
                          text: _pauseText,
                          callback: () {
                            if (_ispause) {
                              _pauseShowDialog(
                                titleText: "주의",
                                titleColor: colorRed,
                                contentText: "일시정지를 해제하겠습니까?",
                                contentColor: Colors.black,
                                callbackCancel: () => Navigator.pop(context),
                                callbackOk: () {
                                  setState(() {
                                    _kickboardText = "킥보드 사용중";
                                    _pauseText = "일시정지";
                                    _ispause = false;
                                    Navigator.pop(context);
                                  });
                                },
                              );
                            } else {
                              _pauseShowDialog(
                                titleText: "주의",
                                titleColor: colorRed,
                                contentText: "일시정지 하여도 요금은 계속 부가됨을 안내드립니다.",
                                contentColor: Colors.black,
                                callbackCancel: () => Navigator.pop(context),
                                callbackOk: () {
                                  setState(() {
                                    _kickboardText = "킥보드 일시정지";
                                    _pauseText = "일시정지 해제";
                                    _ispause = true;
                                    Navigator.pop(context);
                                  });
                                },
                              );
                            }
                          }),
                    ),
                    const ComponentMarginHorizon(enumSize: EnumSize.bigger),
                    Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: ComponentTextBtn(
                          text: '사용종료',
                          callback: () {
                            _pauseShowDialog(
                              titleText: "사용종료",
                              titleColor: colorRed,
                              contentText: '킥보드 이용 요금은 ' +
                                  maskWonFormatter.format(
                                      priceBase + (_usedMin * priceMinute)) +
                                  '원 입니다.\n\n사용 종료를 원하시면 확인 버튼을\n눌러주세요.',
                              contentColor: Colors.black,
                              callbackCancel: () => Navigator.pop(context),
                              callbackOk: () {
                                KickBoardEndRequest kickBoardEndRequest =
                                    KickBoardEndRequest(
                                        _currentPosition!.latitude,
                                        _currentPosition!.longitude);
                                _putUseEnd(historyId, kickBoardEndRequest);
                              },
                            );
                          }),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
