import 'package:flutter/material.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:onboarding/onboarding.dart';

class PageHowToUse extends StatefulWidget {
  const PageHowToUse({super.key});

  @override
  State<PageHowToUse> createState() => _PageHowToUse();
}

class _PageHowToUse extends State<PageHowToUse> {
  late Material materialButton;
  late int index;
  double widthSize = 0.0;
  double heightSize = 0.0;

  @override
  void initState() {
    super.initState();
    materialButton = _skipButton();
    index = 0;
  }

  Material _skipButton({void Function(int)? setIndex}) {
    return Material(
      borderRadius: defaultSkipButtonBorderRadius,
      color: defaultSkipButtonColor,
      child: InkWell(
        borderRadius: defaultSkipButtonBorderRadius,
        onTap: () {
          if (setIndex != null) {
            index = 2;
            setIndex(2);
          }
        },
        child: const Padding(
          padding: defaultSkipButtonPadding,
          child: Text(
            '스킵하기',
            style: defaultSkipButtonTextStyle,
          ),
        ),
      ),
    );
  }

  Material get _signupButton {
    return Material(
      borderRadius: defaultProceedButtonBorderRadius,
      color: defaultProceedButtonColor,
      child: InkWell(
        borderRadius: defaultProceedButtonBorderRadius,
        child: const Padding(
          padding: defaultProceedButtonPadding,
          child: Text(
            '시작하기',
            style: defaultProceedButtonTextStyle,
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final onboardingPagesList = [
      PageModel(
        widget: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.cyan,
            border: Border.all(
              width: 0.0,
              color: background,
            ),
          ),
          child: Container(
            width: MediaQuery.sizeOf(context).width,
            height: MediaQuery.sizeOf(context).height,
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  height: MediaQuery.sizeOf(context).height / 2,
                  padding: EdgeInsets.only(top: 20),
                  child: Container(
                    padding: EdgeInsets.all(100),
                    child: Image.asset(
                      'assets/QRICon.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                ), // QR Code Img
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    '출발하기',
                    style:
                        TextStyle(color: Colors.white, fontSize: fontSizeSuper),
                  ),
                ), // 출발하기
                Container(
                  padding: EdgeInsets.only(top: 20),
                  alignment: Alignment.center,
                  child: Text(
                    'QR코드를 스캔하여 이용할 수 있어요.',
                    style: pageInfoStyle,
                  ),
                ), // QR코드를 스캔하여 이용할 수 있어요.
              ],
            ),
          ),
        ),
      ),
      PageModel(
        widget: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.cyan,
            border: Border.all(
              width: 0.0,
              color: background,
            ),
          ),
          child: Container(
            width: MediaQuery.sizeOf(context).width,
            height: MediaQuery.sizeOf(context).height,
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  height: MediaQuery.sizeOf(context).height / 2,
                  padding: EdgeInsets.only(top: 20),
                  child: Container(
                    padding: EdgeInsets.all(100),
                    child: Image.asset(
                      'assets/parkingfinal.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                ), // 주차 Img
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    '주차하기',
                    style:
                        TextStyle(color: Colors.white, fontSize: fontSizeSuper),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  alignment: Alignment.center,
                  child: Text(
                    '보행자와 자동차의 길을 막지 않도록',
                    style: pageInfoStyle,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  alignment: Alignment.center,
                  child: Text(
                    '인도 가장자리에 주차해주세요.',
                    style: pageInfoStyle,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      PageModel(
        widget: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.cyan,
            border: Border.all(
              width: 0.0,
              color: background,
            ),
          ),
          child: Container(
            width: MediaQuery.sizeOf(context).width,
            height: MediaQuery.sizeOf(context).height,
            padding: EdgeInsets.only(top: 20),
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  height: MediaQuery.sizeOf(context).height / 2,
                  child: Container(
                    padding: EdgeInsets.all(100),
                    child: Image.asset(
                      'assets/promiseIcon.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    '약속하기',
                    style:
                        TextStyle(color: Colors.white, fontSize: fontSizeSuper),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  alignment: Alignment.center,
                  child: Text(
                    '헬멧 착용하기',
                    style: pageInfoStyle,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  alignment: Alignment.center,
                  child: Text(
                    '주차구역 준수',
                    style: pageInfoStyle,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  alignment: Alignment.center,
                  child: Text(
                    '음주운전 금지',
                    style: pageInfoStyle,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  alignment: Alignment.center,
                  child: Text(
                    '2인 탑승금지',
                    style: pageInfoStyle,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ];

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        body: Onboarding(
          pages: onboardingPagesList,
          onPageChange: (int pageIndex) {
            index = pageIndex;
          },
          startPageIndex: 0,
          footerBuilder: (context, dragDistance, pagesLength, setIndex) {
            return DecoratedBox(
              decoration: BoxDecoration(
                color: background,
                border: Border.all(
                  width: 0.0,
                  color: background,
                ),
              ),
              child: ColoredBox(
                color: background,
                child: Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomIndicator(
                        netDragPercent: dragDistance,
                        pagesLength: pagesLength,
                        indicator: Indicator(
                          indicatorDesign: IndicatorDesign.polygon(
                            polygonDesign: PolygonDesign(
                              polygon: DesignType.polygon_pentagon,
                            ),
                          ),
                        ),
                      ),
                      index == pagesLength - 1
                          ? _signupButton
                          : _skipButton(setIndex: setIndex)
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
