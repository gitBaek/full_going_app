import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/components/component_divider.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';

class PageInfo extends StatefulWidget {
  const PageInfo({super.key});

  @override
  State<PageInfo> createState() => _PageInfoState();
}

class _PageInfoState extends State<PageInfo> {
  BackButtonBehavior backButtonBehavior = BackButtonBehavior.none;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "고객센터"),
      body: Container(
        padding: bodyPaddingLeftRight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const ComponentMarginVertical(enumSize: EnumSize.big),
            const Text("무엇을 도와드릴까요 ?",
                style: TextStyle(
                    fontSize: fontSizeSuper, fontWeight: FontWeight.bold)),
            const ComponentMarginVertical(),
            ComponentDivider(),
            const ComponentMarginVertical(
              enumSize: EnumSize.mid,
            ),
            const Text("고객센터 상담시간",
                style: TextStyle(
                    fontSize: fontSizeBig, fontWeight: FontWeight.w700)),
            const ComponentMarginVertical(enumSize: EnumSize.big),
            const Text("전화상담 : 평일 09:00 - 18:00",
                style: TextStyle(
                  fontSize: fontSizeMid,
                  fontWeight: FontWeight.w700,
                )),
            const ComponentMarginVertical(),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text("점심시간 12:00 - 14:00 / 주말 공휴일 휴무",
                  style: TextStyle(fontSize: fontSizeSm)),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("채팅상담 : 09:30 - 익일 01:00",
                style: TextStyle(
                    fontSize: fontSizeMid, fontWeight: FontWeight.w700)),
            const ComponentMarginVertical(),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text("점심시간 12:00 - 14:00 / 공휴일 휴무",
                  style: TextStyle(fontSize: fontSizeSm)),
            ),
            const SizedBox(
              height: 100,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                text: "채팅 상담하기",
                iconData: Icons.wechat_rounded,
                textColor: Colors.white,
                borderColor: colorPrimary,
                bgColor: colorPrimary,
                callback: () {
                  _showDialog('채팅 상담', '채팅 상담하기');
                },
              ),
            ),
            const ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                text: "전화 상담하기",
                iconData: Icons.call,
                textColor: Colors.white,
                borderColor: colorGreen,
                bgColor: colorGreen,
                callback: () {
                  _showDialog('전화 상담', '전화 상담하기');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showDialog(String title, String text) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Column(
              children: <Widget>[
                Text(title),
              ],
            ),
            //
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(text),
              ],
            ),
            actions: <Widget>[
              TextButton(
                child: Text("확인"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text("취소"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}
