import 'dart:io';

import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/config/config_api.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/functions/token_lib.dart';
import 'package:full_going_app/model/my_profile_status_item_result.dart';
import 'package:full_going_app/pages/profile/page_license_reg.dart';
import 'package:full_going_app/pages/profile/page_profile_fix.dart';
import 'package:full_going_app/repository/repo_member.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

class PageProfile extends StatefulWidget {
  const PageProfile({super.key});

  @override
  State<PageProfile> createState() => _PageProfileState();
}

class _PageProfileState extends State<PageProfile> {
  final ImagePicker _picker = ImagePicker();

  String _directoryPath = "";
  String _defaultPath = "";
  String username = "";
  String phoneNumber = "";
  bool isLicence = false;
  XFile? _pickedImage;

  Future<void> _loadProfileData() async {
    MyProfileStatusItemResult result = await RepoMember().getList();

    setState(() {
      username = result.data.username;
      phoneNumber = result.data.phoneNumber;
      isLicence = result.data.isLicence;
    });
  }

  // 카메라, 갤러리에서 이미지 1개 불러오기
  // ImageSource.galley , ImageSource.camera
  void getImage(ImageSource source) async {
    XFile? image = null;
    image = await _picker.pickImage(source: source);
    if (image != null) {
      setState(() {
        _doSaveProfilePath(image);
        _pickedImage = image;
      });
    }
  }

  Future<void> initProfilePath() async {
    final Directory appDocumentsDir = await getApplicationDocumentsDirectory();
    _directoryPath = appDocumentsDir.path;
    _defaultPath = "$_directoryPath/profile.jpa";
    final File tempFile = File(_defaultPath);
    if (tempFile.existsSync()) {
      _pickedImage = XFile(_defaultPath);
    }
  }

  Future<void> _doSaveProfilePath(XFile? pickedImage) async {
    File tmpFile = File(pickedImage!.path);
    // final Directory appDocumentsDir = await getApplicationDocumentsDirectory();
    // final String directoryPath = appDocumentsDir.path;
    // final String fileName = pickedImage!.name;
    // final String savePath = "$directoryPath/$fileName";

    // print("앱 경로");
    // print(appDocumentsDir.path);

    //print("파일 경로");
    //print(pickedImage!.path);
    //print(pickedImage!.name);

    tmpFile = await tmpFile.copy(_defaultPath);
    print("저장");
    print(_defaultPath);
  }

  Future<void> _doDelProfilePath(String path) async {
    print("삭제");
    File tempFile = File(path);
    if (tempFile.existsSync()) tempFile.delete();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initProfilePath();
    _loadProfileData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '프로필',
      ),
      body: SingleChildScrollView(
          child: Container(
        padding: bodyPaddingLeftRight,
        child: Column(
          children: [
            const ComponentMarginVertical(enumSize: EnumSize.big),
            _pickedImage != null
                ? CircleAvatar(
                    maxRadius: (MediaQuery.of(context).size.width / 2 - 120),
                    backgroundImage: FileImage(File(_pickedImage!.path)),
                    child: _profileIcon(),
                  )
                : CircleAvatar(
                    maxRadius: (MediaQuery.of(context).size.width / 2 - 120),
                    backgroundImage:
                        const AssetImage('assets/img_default_profile.png'),
                    backgroundColor: Colors.white,
                    child: _profileIcon(),
                  ),
            const ComponentMarginVertical(
              enumSize: EnumSize.big,
            ),
            buildProfile("이름", username),
            const ComponentMarginVertical(),
            buildProfile("연락처", phoneNumber),
            const ComponentMarginVertical(),
            isLicence
                ? buildProfile("면허유무", "등록")
                : buildProfile("면허유무", "미등록"),
            const ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                text: "비밀번호 변경",
                textColor: Colors.white,
                borderColor: colorPrimary,
                callback: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const PageProfileFix()));
                },
              ),
            ), // 비밀번호 변경 버튼
            const ComponentMarginVertical(),
            isLicence
                ? Container()
                : Container(
                    padding: EdgeInsets.only(bottom: 10),
                    width: MediaQuery.of(context).size.width,
                    child: ComponentTextBtn(
                      text: "운전면허 등록",
                      textColor: Colors.white,
                      borderColor: colorPrimary,
                      callback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageLicenseReg())).then(
                          (value) {
                            _loadProfileData();
                          },
                        );
                      },
                    ),
                  ), // 운전면허 등록 버튼
            //const ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                text: "로그아웃",
                textColor: Colors.white,
                borderColor: colorPrimary,
                callback: () {
                  TokenLib.logout(context);
                },
              ),
            ), // 로그아웃 버튼
            const ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                text: "사진 초기화",
                textColor: Colors.white,
                borderColor: colorPrimary,
                callback: () {
                  print("사진 초기화");
                  setState(() {
                    _doDelProfilePath(_defaultPath);
                    _pickedImage = null;
                  });
                },
              ),
            ),
          ],
        ),
      )),
    );
  }

  Column buildProfile(String title, String textVal) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title, style: const TextStyle(fontSize: fontSizeMid)),
            Text(textVal, style: const TextStyle(fontSize: fontSizeMid)),
          ],
        ),
        const ComponentMarginVertical(),
        const Divider(
          thickness: 2,
          height: 1,
          color: colorLightGray,
        ),
      ],
    );
  }

  Stack _profileIcon() {
    return Stack(children: [
      Container(
          alignment: Alignment.bottomRight,
          child: CircleAvatar(
            radius: 28,
            backgroundColor: Colors.blue,
            child: IconButton(
              icon: const Icon(Icons.camera_alt_outlined,
                  color: Colors.white, size: 25),
              alignment: Alignment.center,
              onPressed: () {
                _showBottomSheet();
              },
            ),
          ))
    ]);
  }

  _showBottomSheet() {
    return showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  TextButton.icon(
                    icon: const Icon(
                      Icons.camera_alt,
                      size: 50,
                    ),
                    label: const Text("Camera"),
                    onPressed: () => getImage(ImageSource.camera),
                  ),
                  TextButton.icon(
                    icon: const Icon(
                      Icons.photo_library,
                      size: 50,
                    ),
                    label: const Text("Gallery"),
                    onPressed: () => getImage(ImageSource.gallery),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
