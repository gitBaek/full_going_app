class MyProfileStatusItem {
  String dateCreate;
  bool isLicence;
  String name;
  String phoneNumber;
  num remainMileage;
  num remainPass;
  num remainPrice;
  String username;

  MyProfileStatusItem(this.dateCreate, this.isLicence, this.name, this.phoneNumber, this.remainMileage, this.remainPass, this.remainPrice, this.username);

  factory MyProfileStatusItem.fromJson(Map<String, dynamic> json) {
    return MyProfileStatusItem(
      json['dateCreate'],
      json['isLicence'],
      json['name'],
      json['phoneNumber'],
      json['remainMileage'],
      json['remainPass'],
      json['remainPrice'],
      json['username'],
    );
  }
}
