class MyChargeItem {
  num charge;
  String chargeType;
  String dateReg;
  num paymentAmount;
  num resultMileage;

  MyChargeItem(this.charge, this.chargeType, this.dateReg, this.paymentAmount, this.resultMileage);

  factory MyChargeItem.fromJson(Map<String, dynamic> json) {
    return MyChargeItem(
      json['charge'],
      json['chargeType'],
      json['dateReg'],
      json['paymentAmount'],
      json['resultMileage']
    );
  }
}
