import 'package:full_going_app/model/my_history_item.dart';

class MyHistoryListResult {
  // isSuccess랑 msg, code는 안쓸거라서 줘도 안받음.
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<MyHistoryItem>? list;

  // 중간에 {this.list}는 list가 있을수도 있고 없을수도 있으니까 ?<- 이 기호랑 짝꿍으로
  // 있을수도 있고 없을수도 있다 처리해주는거임.
  MyHistoryListResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory MyHistoryListResult.fromJson(Map<String, dynamic> json) {
    return MyHistoryListResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => MyHistoryItem.fromJson(e)).toList() : [],
    );
  }
}